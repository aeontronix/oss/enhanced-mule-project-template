mkdir -p target/test
cd target/test
mvn dependency:copy -DlocalRepositoryDirectory=libs -Dartifact=com.aeontronix.enhanced-mule:enhanced-mule-project-template:0.9.0-SNAPSHOT -DoutputDirectory=. -Dmdep.stripVersion=true
mvn com.aeontronix.genesis:genesis-maven:0.9.2:template -Dgenesis.template=enhanced-mule-project-template.jar -Dgenesis.target=. -Dgenesis.delete=true
